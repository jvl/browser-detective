if(!document.all){
    if('MozTransform' in document.body.style){
        //Firefox
        var elements = document.getElementsByTagName('html');
        elements[0].className += " firefox";
    }
    if(window.chrome){
        //Chrome/Chromium
        var elements = document.getElementsByTagName('html');
        elements[0].className += " chrome";
    }
    if(window.opera){
        //Opera
        var elements = document.getElementsByTagName('html');
        elements[0].className += " opera";
    }
    if('WebkitTransform' in document.body.style){
        //Webkit derived browsers
        var elements = document.getElementsByTagName('html');
        elements[0].className += " webkit";
    }
}
else if(document.all){
    //Do IE version tests
	if(window.atob){
    	//IE10
    	var elements = document.getElementsByTagName('html');
    	elements[0].className += " IE10";
	}
	else if(document.getElementsByClassName){
    	//IE9
    	var elements = document.getElementsByTagName('html');
    	elements[0].className += " IE9";
	}
	else if(document.querySelector){
		//IE8
		var elements = document.getElementsByTagName('html');
		elements[0].className += " IE8";
	}
    else if(window.XMLHttpRequest){
    	//IE7
    	var elements = document.getElementsByTagName('html');
    	elements[0].className += " IE7";
    }
    else if(document.compatMode){
    	//IE6
    	var elements = document.getElementsByTagName('html');
    	elements[0].className += " IE6";
    }
    else if(window.createPopup){
    	//IE5.5
    	var elements = document.getElementsByTagName('html');
    	elements[0].className += " IE55";
    }
    else if(window.attachEvent){
    	//IE5.0.1
    	var elements = document.getElementsByTagName('html');
    	elements[0].className += " IE501";
    }
    else if(document.images){
    	//IE4
    	window.location.replace(" http://www.december.com/html/demo/hello.html" );
    }
}
else{
	var elements = document.getElementsByTagName('html');
	elements[0].className += " unknown";
}