Browser Detective

What is it?
A fork of IE Detective to add support for detecting additional browsers. (E.g. notIE)

How do I use it?
Include the file like any other script in your <body> tag:

<script type="text/javascript" src="js/browser-detective.js"></script>

What IE versions are supported?
IE 3+ - every version that supports JavaScript!

What classes are added to the <html> tag?
unknown
webkit
chrome
firefox
opera
IE10
IE9
IE8
IE7
IE6
IE55
IE501

Note: IE4 does not support classes on the <html> tag.
      You can, however redirect users to another page.